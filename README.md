Pocket Hero
===========

This project folder was created using GB Studio and is tested/working with version 3.1.0 of the application. Pocket Hero can be modified and compiled using GB Studio application and tools.

Pocket Hero is an application for use with OpenD6, an adaptable tabletop role-playing game system published in 1996. The officially released software can be found at the application's [Itch.io page](https://radicalgnu.itch.io/pocket-hero). An [introductory scenario](https://webs.radicalgnu.xyz/opend6/pocket-hero-intro.html) can be found at the developer's website.


Credits
-------

Several artists contributed works to the commons which are used in Pocket Hero.

OpenD6 was released by Purgatory Publishing in 2009 as Open Game Content under the terms of the Open Game License.
[OpenD6 and the Open Game License](https://ogc.rpglibrary.org/index.php?title=OpenD6:OpenD6_and_the_Open_Game_License)

Dice sprites are from
["Dice collection D6-D10 16x16px"](https://opengameart.org/content/dice-collection-d6-d10-16x16px)
by [Refeuh](https://opengameart.org/users/refeuh)
licensed [OGA-BY 3.0](https://opengameart.org/content/oga-by-30-faq)

Die code font is from
["32x32 fantasy tileset"](https://opengameart.org/content/32x32-fantasy-tileset)
by [Jerom](http://jerom-bd.blogspot.com/)
licensed [CC-BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/)

[GB Studio assets](https://www.gbstudio.dev/)
licensed [MIT](https://github.com/chrismaltby/gb-studio/)

Title screen "Fight 16" font is from
["New original Grafx2 font collection"](https://opengameart.org/content/new-original-grafx2-font-collection)
by [devurandom](https://opengameart.org/users/devurandom)
licensed [CC0](https://creativecommons.org/publicdomain/zero/1.0/)
